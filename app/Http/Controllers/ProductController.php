<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use PHPUnit\Util\Json;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * @return string
     */
    public function show(): string
    {
        $products = Product::all();
        return $products->toJson();
    }

    public function add() {
       $product = new Product();
       $product->name = Str::random(10);
       $product->articleID = Str::random(10);
       $product->price = rand(0, 100);
       $product->width = rand(0, 100);
       $product->height = rand(0, 100);
       $product->description = Str::random(10);
        $product->save();
    }
}
